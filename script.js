const menuIcon = document.querySelector('.menu-icon');
if(menuIcon){
  const className = ['active'];
  menuIcon.addEventListener('click',function(e){
    const nawBurger = document.querySelector('.naw-burger')
    menuIcon.classList.toggle(className[0]);
    nawBurger.classList.toggle(className[0]);
  });
}